'use strict';

describe('Unit test: App Controller', () => {

    let scope, controller;

    beforeEach(module('app'));

    beforeEach(inject((_$controller_, User) => {
        controller = _$controller_;
        scope      = {};
        controller = controller('appCtrl', { $scope : scope });
    }));

    it('scope should have an instance of new User', () => {
        expect(scope.user.firstName).toEqual(null);
        expect(scope.user.lastName).toEqual(null);
        expect(scope.user.annualSalary).toEqual(0);
        expect(scope.user.superRate).toEqual(0);
        expect(scope.user.paymentStartDate).toEqual(null);
    });
});