'use strict';

describe('Unit test: Model User', () => {

    let user;

    beforeEach(module('app'));

    beforeEach(inject((User) => {
        user = new User('Jane', 'Doe', 120.00, 9, '01 January - 31 January');
    }));

    it('user should have these attributes', () => {
        expect(user).toBeDefined();
        expect(user.firstName).toBeDefined();
        expect(user.lastName).toBeDefined();
        expect(user.annualSalary).toBeDefined();
        expect(user.superRate).toBeDefined();
        expect(user.paymentStartDate).toBeDefined();
    });

    it('user should have these values', () => {
        expect(user.firstName).toEqual('Jane');
        expect(user.lastName).toEqual('Doe');
        expect(user.annualSalary).toEqual(120.00);
        expect(user.superRate).toEqual(9);
        expect(user.paymentStartDate).toEqual('01 January - 31 January');
    });

    it('user should have these methods', () => {
        expect(user._roundOff()).toBeDefined();
        expect(user.getFullName()).toBeDefined();
        expect(user.getPayPeriod()).toBeDefined();
        expect(user.getGrossIncome()).toBeDefined();
        expect(user.getIncomeTax()).toBeDefined();
        expect(user.getNetIncome()).toBeDefined();
        expect(user.getSuperRate()).toBeDefined();
    });

    describe('Method _roundOff', () => {
        it('show round up when it greater than or equal to 0.5', () => {
            for(let i = 10.5; i < 11; i = i + 0.01) {
                expect(user._roundOff(i)).toBe(11);
            }
        });

        it('show round down when it less than 0.5', () => {
            for(let i = 10.49; i > 10; i = i - 0.01) {
                expect(user._roundOff(i)).toBe(10);
            }
        });
    });

    describe('Method getFullName', () => {
        it('should return fullname when both firstname and lastname exist', () => {
            expect(user.getFullName()).toBe('Jane Doe');
        });

        it('should return default value when only firstname exist', () => {
            user.lastName = null;
            expect(user.getFullName()).toBe('-');
        });

        it('should return default value when only lastname exist', () => {
            user.firstName = null;
            expect(user.getFullName()).toBe('-');
        });
    });

    describe('Method getPayPeriod', () => {
        it('should return paymentStartDate when it exists', () => {
            expect(user.getPayPeriod()).toBe('01 January - 31 January');
        });

        it('should return default value when only paymentStartDate exist', () => {
            user.paymentStartDate = null;
            expect(user.getPayPeriod()).toBe('-');
        });

        it('should return anything that paymentStartDate has been set', () => {
            user.paymentStartDate = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
            expect(user.getPayPeriod()).toBe('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');
        });
    });

    describe('Method getGrossIncome', () => {
        it('should return Gross Income', () => {
            expect(user.getGrossIncome()).toBe(10);
        });

        it('should return Gross Income with round down value', () => {
            for(let i = 120.00 ; i < 120.5 ; i = i + 0.01) {
                user.annualSalary = i;
                expect(user.getGrossIncome()).toBe(10);
            }
        });

        it('should return Gross Income with round up value', () => {
            for(let i = 120.5 ; i < 121 ; i = i + 0.01) {
                user.annualSalary = i;
                expect(user.getGrossIncome()).toBe(10);
            }
        });
    });

    describe('Method getIncomeTax', () => {
        it('test negative value', () => {
            user.annualSalary = -1;
            expect(() => user.getIncomeTax()).toThrow(new Error('Value must be greater than zero'));
        });

        it('test alphabet', () => {
            user.annualSalary = 'abcdefghijklmnopqrstuvwxyz';
            expect(() => user.getIncomeTax()).toThrow(new Error('Value must be greater than zero'));
        });

        it('should calculate income tax between 0 and 18200', () => {
            for(let i = 0 ; i < 18201 ; ++i) {
                user.annualSalary = i;
                expect(user.getIncomeTax()).toBe(0);
            }
        });

        it('should calculate income tax between 18201 and 37000', () => {
            for(let i = 18201 ; i < 37001 ; ++i) {
                user.annualSalary = i;
                expect(user.getIncomeTax()).toBe(Math.round((((user.annualSalary - 18200) * 0.19) / 12)));
            }
        });

        it('should calculate income tax between 37001 and 80000', () => {
            for(let i = 37001 ; i < 80001 ; ++i) {
                user.annualSalary = i;
                expect(user.getIncomeTax()).toBe(Math.round(((((user.annualSalary - 37000) * 0.325) + 3572) / 12)));
            }
        });

        it('should calculate income tax between 80001 and 180000', () => {
            for(let i = 80001 ; i < 180000 ; ++i) {
                user.annualSalary = i;
                expect(user.getIncomeTax()).toBe(Math.round(((((user.annualSalary - 80000) * 0.37) + 17547) / 12)));
            }
        });

        it('should calculate income tax for more than 180001', () => {
            for(let i = 180001 ; i < 200000 ; ++i) {
                user.annualSalary = i;
                expect(user.getIncomeTax()).toBe(Math.round(((((user.annualSalary - 180000) * 0.45) + 54547) / 12)));
            }
        });
    });

    describe('Method getNetIncome', () => {
        it('should return net income', () => {
            expect(user.getNetIncome()).toBe(user.getGrossIncome() - user.getIncomeTax());
        });
    });

    describe('Method getSuperRate', () => {
        it('should return super rate', () => {
            expect(user.getSuperRate()).toBe(Math.round(user.getGrossIncome() * (user.superRate / 100)));
        });
    });
});