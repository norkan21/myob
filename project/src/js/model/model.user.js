'use strict';

angular.module('app')
    .factory('User', [() => {
        return class User {
            constructor(firstName = null, lastName = null, annualSalary = 0, superRate = 0, paymentStartDate = null) {
                this.firstName        = firstName;
                this.lastName         = lastName;
                this.annualSalary     = annualSalary;
                this.superRate        = superRate; // Rate is input as percentage
                this.paymentStartDate = paymentStartDate;
            }

            /**
             * Method roundOff
             * Use to round off value to the nearest dollar when >= 50 cents otherwise round down
             * e.g if 120.50 => 121
             * e.g if 120.49 => 120
            **/
            _roundOff(val) {
                return Math.round(val);
            }

            /**
             * Method getFullName
             * return firstname and lastname
            **/
            getFullName() {
                return (this.firstName && this.lastName) ? `${this.firstName} ${this.lastName}` : '-';
            }

            /**
             * Method getPayPeriod
             * return paymentStartDate
            **/
            getPayPeriod() {
                return this.paymentStartDate || '-';
            }

            /**
             * Method getGrossIncome
             * return A round off value of the gross income
            **/
            getGrossIncome() {
                return this._roundOff(this.annualSalary / 12);
            }

            /**
             * Method getIncomeTax
             * return income tax within the right range
            **/
            getIncomeTax() {
                if(this.annualSalary >= 0 && this.annualSalary <= 18200) {
                    return 0;
                } else if(this.annualSalary >= 18201 && this.annualSalary <= 37000) {
                    return this._roundOff(((this.annualSalary - 18200) * 0.19) / 12);
                } else if(this.annualSalary >= 37001 && this.annualSalary <= 80000) {
                    return this._roundOff((((this.annualSalary - 37000) * 0.325) + 3572) / 12);
                } else if(this.annualSalary >= 80001 && this.annualSalary <= 180000) {
                    return this._roundOff((((this.annualSalary - 80000) * 0.37) + 17547) / 12);
                } else if(this.annualSalary >= 180001) {
                    return this._roundOff((((this.annualSalary - 180000) * 0.45) + 54547) / 12);
                } else {
                    throw new Error('Value must be greater than zero');
                }
            }

            /**
             * Method getNetIncome
             * return net income
            **/
            getNetIncome() {
                return this.getGrossIncome() - this.getIncomeTax();
            }

            /**
             * Method getSuperRate
             * return super rate
            **/
            getSuperRate() {
                return this._roundOff(this.getGrossIncome() * (this.superRate / 100));
            }
        }
    }]);