# README #

Code test for MYOB

### What is this repository build on? ###

* MacOS Sierra v10.12
* Vagrant v1.8.4
* VirtualBox v5.0.26
* node v6.9.1
* npm v3.10.8

### How do I get set up? ###

* Download and install [vagrant]
* Download and install [virtualbox]
* Clone the code test at [project] using git
    
    ```
    git clone git@bitbucket.org:norkan21/myob.git
    ```

### Running the project ###

* Use a terminal and navigate to project
    
    ```
    cd path/to/folder
    ```
    
* Install dependencies, compile the source code and run the tests
    
    ```
    vagrant up
    ```

* Start the server

    ```
    vagrant ssh
    ```
   
    password is 'vagrant'

    ```
    npm start
    ```

* Open the browser and navigate to


    ```
    http://localhost:8080/
    ```

* Useful commands

    
    ```
    npm install - Install packages
    ```

    ```
    npm start   - Start the server
    ```

    ```
    npm test    - Start the test
    ```

    ```
    gulp        - Compile the source code
    ```

    ```
    gulp development - Same as gulp but with a watch
    ```


### Who do I talk to? ###
Eric Leung

[vagrant]: https://www.vagrantup.com/downloads.html
[virtualbox]: https://www.virtualbox.org/wiki/Downloads
[project]: https://bitbucket.org/norkan21/myob