'use strict';

/**
 * Module app
**/
angular.module('app')
    .controller('appCtrl', ['$scope', 'User', ($scope, User) => {
        $scope.user = new User();
    }]);