'use strict';

/**
* Class tasks
* Provide a list of methods that can be used with gulp
**/

const babel    = require('gulp-babel'),
      cleanCSS = require('gulp-clean-css'),
      concat   = require('gulp-concat'),
      del      = require('del'),
      gulp     = require('gulp'),
      uglify   = require('gulp-uglify');

class task {

    static init() {
        //JavaScript libraries
        gulp.src([
                'node_modules/jquery/dist/jquery.js',
                'node_modules/bootstrap/dist/js/bootstrap.js',
                'node_modules/angular/angular.js',
                'node_modules/angular-route/angular-route.js'
            ])
            .pipe(concat('library.js'))
            .pipe(uglify())
            .pipe(gulp.dest('public/library/js'));

        //CSS libraries
        gulp.src([
                'node_modules/bootstrap/dist/css/bootstrap.css',
            ])
            .pipe(concat('library.css'))
            .pipe(cleanCSS({compatibility: 'ie9'}))
            .pipe(gulp.dest('public/library/css'));

        //Fonts
        gulp.src([
                'node_modules/bootstrap/dist/fonts/*',
            ])
            .pipe(gulp.dest('public/library/fonts'));
    }

    /**
     * Method css
     * Deal only with css
     *
     * Note : A pre-processor will not be used as there are too little styling
    **/
    static css() {
      gulp.src('src/**/*.css')
            .pipe(concat('app.css'))
            .pipe(gulp.dest('public/css'));
    }

    /**
     *  Method javascripts
     *  Deal only with javascripts
    **/
    static javascript() {
        gulp.src('src/**/*.js')
            .pipe(concat('app.js'))
            .pipe(babel())
            .pipe(gulp.dest('public/js'));
    }

    /**
     *  Method view
     *  Deal only with html
    **/
    static view() {
        gulp.src('src/**/**/*.html')
        .pipe(gulp.dest('public'));
    }

    /**
     * Method clean
     * Delete all files and subfolders in public
     * Clean compile the files when gulp is executed
     **/
    static clean() {
        del(['public'], {dryRun: true}).then(paths => {
            console.log('Deleted files and folders:\n', paths.join('\n'));
        });
    }
}

module.exports = task;