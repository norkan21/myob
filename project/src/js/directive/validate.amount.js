'use strict';

angular.module('app')
    .directive('validateAmount', () => {
        return {
            require : 'ngModel',
            link    : (scope, elm, attrs, ctrl) => {
                let pattern = /^[1-9][\d]*(\.[\d]{1,2})?$/;
                ctrl.$validators.validateAmount = (modelValue, viewValue) => {
                    return (ctrl.$isEmpty(modelValue) || pattern.test(viewValue)) ? true : false;
                }
            }
        }
    });