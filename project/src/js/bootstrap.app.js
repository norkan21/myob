'use strict';

angular.module('app', ['ngRoute'])
    .config(['$routeProvider', '$locationProvider', ($routeProvider, $locationProvider) => {
        $locationProvider.html5Mode(true);

        $routeProvider.when('/', {
            templateUrl : 'view/app.html',
            controller  : 'appCtrl'
        });
    }]);