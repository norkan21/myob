'use strict';

const express    = require('express'),
      app        = express(),
      exphbs     = require('express-handlebars'),
      path       = require('path');

// view engine setup
app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

// Reset the 'public' folder to 'dist' when accessing from web browser
app.use(express.static(path.join(__dirname, 'public')));

// Routes
app.use('/', require('./routes/home')); //ONLY one route for the code test

// catch 404 and forward to error handler
app.use((req, res, next) => {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// development error handler
if (app.get('env') === 'development') {
  app.use((err, req, res, next) => {
    res.status(err.status || 500);
    res.send(err.message);
  });
}

module.exports = app;
