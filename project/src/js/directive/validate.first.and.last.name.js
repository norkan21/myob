'use strict';

angular.module('app')
    .directive('validateFirstAndLastName', () => {
        return {
            require : 'ngModel',
            link    : (scope, elm, attrs, ctrl) => {
                let pattern = /^[a-z][a-z\s]+$/i;
                ctrl.$validators.validateFirstAndLastName = (modelValue, viewValue) => {
                    return (ctrl.$isEmpty(modelValue) || pattern.test(viewValue)) ? true : false;
                }
            }
        }
    });